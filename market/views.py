from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.views.generic import View
from .models import User


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def lio(request):
    return render(request, 'market/lio.html')

class SignUp(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'market/signup.html')

    def post(self, request, *args, **kwargs):
        username = request.post['username']
        email = request.post['email']
        passwd = request.post['passwd']
        phone_number = request.post['phone_number']
        pass


# market
def market_home(request):
    recoms = [
        {'name': 'DATA WING', 'img': '03.png', 'cost': 'FREE'},
        {'name': 'iTaxi', 'img': '05.png', 'cost': 'FREE'},
        {'name': 'Home Workout', 'img': '04.png', 'cost': '2.9$'},
        {'name': 'Lingua Player', 'img': '02.png', 'cost': 'FREE'},
    ]

    tops = [
        {'name': 'Swipe Brick', 'img': '06.png', 'cost': 'FREE'},
        {'name': 'Shadow Fight 3', 'img': '07.png', 'cost': 'FREE'},
        {'name': 'Chilly Snow', 'img': '08.png', 'cost': '2.9$'},
        {'name': 'Rubik\'s Cube', 'img': '09.png', 'cost': 'FREE'},
    ]

    tus = [
        {'name': 'VPN Free', 'img': '10.png', 'cost': 'FREE'},
        {'name': 'VPN Monster', 'img': '11.png', 'cost': 'FREE'},
        {'name': 'Lantern', 'img': '12.png', 'cost': '2.9$'},
        {'name': 'Hi VPN', 'img': '13.png', 'cost': 'FREE'},
    ]

    return render(request, 'market/market_home.html', {'recoms': recoms, 'tops': tops, 'tus': tus})

# developer
def developer_panel_home(request):
    return render(request, 'market/dp_home.html', {'home': 1})

def developer_panel_applications(request):
    apps = [
        {'name': 'app1', 'type': 'type1', 'category': 'cat1', 'status': 'published', 'downloads': '1234'},
        {'name': 'app2', 'type': 'type2', 'category': 'cat2', 'status': 'changes required', 'downloads': '2345'},
        {'name': 'app3', 'type': 'type3', 'category': 'cat3', 'status': 'waitting', 'downloads': '3456'}
    ]
    return render(request, 'market/dp_applications.html', {'applications': 1, 'apps': apps})


def developer_panel_add_application(request):
    apps = [
        {'name': 'app1', 'type': 'type1', 'category': 'cat1', 'status': 'published', 'downloads': '1234'},
        {'name': 'app2', 'type': 'type2', 'category': 'cat2', 'status': 'changes required', 'downloads': '2345'},
        {'name': 'app3', 'type': 'type3', 'category': 'cat3', 'status': 'waitting', 'downloads': '3456'}
    ]
    return render(request, 'market/dp_add_application.html', {'applications': 1, 'apps': apps})

def developer_panel_device_catalog(request):
    return render(request, 'market/dp_device_catalog.html', {'device_catalog': 1})

def developer_panel_statistics(request):
    return render(request, 'market/dp_statistics.html', {'statistics': 1})

def developer_panel_financial(request):
    return render(request, 'market/dp_financial.html', {'financial': 1})

def developer_panel_support(request):
    return render(request, 'market/dp_support.html', {'support': 1})

def developer_panel_setting(request):
    return render(request, 'market/dp_setting.html', {'setting': 1})