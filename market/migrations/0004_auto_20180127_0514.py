# Generated by Django 2.0.1 on 2018-01-27 05:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0003_auto_20180124_0808'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='crash_count',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='application',
            name='install_count',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='application',
            name='unistall_count',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='category',
            name='category_type',
            field=models.CharField(default='', max_length=20),
        ),
        migrations.AlterField(
            model_name='application',
            name='publish_status',
            field=models.CharField(default='Waiting', max_length=20),
        ),
    ]
