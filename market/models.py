from django.db import models
from django.core.exceptions import ValidationError


class Admin(models.Model):
    username = models.CharField(max_length=80, unique=True, blank=False)
    email = models.CharField(max_length=120, unique=True)
    passwd = models.CharField(max_length=120, blank=False)
    # passwd_salt = models.CharField(max_length=120, blank=False)

    def __str__(self):
        return self.username

class Support(models.Model):
    username = models.CharField(max_length=50, unique=True, blank=False)
    name = models.CharField(max_length=50, blank=False)
    email = models.CharField(max_length=80, unique=True, blank=False)
    role = models.CharField(max_length=30, blank=False)
    passwd = models.CharField(max_length=120, blank=False)
    # passwd_salt = models.CharField(max_length=120, blank=False)

    def clean(self):
        if not role in ('translate_service', 'developer_service', 'user_service'):
            raise ValueError('role must be translate_service, developer_service or user_service')


    def __str__(self):
        return self.username



class Developer(models.Model):
    username = models.CharField(max_length=50, blank=False, unique=True)
    name = models.CharField(max_length=50, blank=False)
    email = models.CharField(max_length=120, blank=False, unique=True)
    phone_number = models.CharField(max_length=13, blank=False, unique=True)
    passwd = models.CharField(max_length=120, blank=False)
    # paswd_salt = models.CharField(max_length=120, blank=False)
    national_id = models.CharField(max_length=10, blank=False)
    national_picture = models.CharField(max_length=120)
    zip_code = models.CharField(max_length=15)
    register_date = models.DateField(auto_now_add=True, blank=False)
    # address

    def __str__(self):
        return self.username

class Category(models.Model):
    name = models.CharField(max_length=80, blank=False, unique=True)
    category_type = models.CharField(max_length=20, blank=False, default='')

    def clean(self):
        if not self.category_type in ('app', 'game'):
            raise ValueError('role must be app or game')

    def __str__(self):
        return self.name + ' : ' + self.category_type

class Application(models.Model):
    application_name = models.CharField(max_length=80, blank=False)
    developer = models.ForeignKey(Developer, on_delete=models.CASCADE)
    application_type = models.CharField(max_length=80, blank=False)
    categories = models.ManyToManyField(Category)
    package_name = models.CharField(max_length=80, blank=False, unique=True)
    version_code = models.CharField(max_length=20, blank=False)
    app_file = models.CharField(max_length=300, blank=False)
    # Icon handled in Icon
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=False, default=0)
    in_app_payment = models.BooleanField(blank=False, default=False)
    publish_status = models.CharField(max_length=20, blank=False, default='Waiting')
    # Media handled in Media
    banner = models.CharField(max_length=300)
    ad_status = models.BooleanField(blank=False, default=False)
    age_classification = models.CharField(max_length=2, blank=False, default='D')
    # Keyword handled in Keyword
    website = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    install_count = models.IntegerField(default=0)
    unistall_count = models.IntegerField(default=0)
    crash_count = models.IntegerField(default=0)
    # reviews = models.CharField(db.String(120), blank=False)
    # handled in revies
    # dont implement
    # locales = db.Column(db.String(120), blank=False)
    # analytics = db.Column(db.String(120), blank=False)

    def clean(self):
        if not self.application_type in ('app', 'game'):
            raise ValidationError('Application type must be app or game')
        if not self.publish_status in ('Published', 'Waiting', 'Removed', 'Changes Required'):
            raise ValidationError('Application publish_status must be Published, Waiting, Removed, Changes Required')
        if not self.age_classification in ('A', 'B', 'C', 'D'):
            raise ValidationError('Age classification must be A, B, C or D')


    def __str__(self):
        return self.application_name



class User(models.Model):
    username = models.CharField(max_length=50, blank=False, unique=True)
    name = models.CharField(max_length=50, blank=False)
    email = models.CharField(max_length=100, unique=True)
    phone_number = models.CharField(max_length=13, unique=True)
    passwd = models.CharField(max_length=120, blank=False)
    # passwd_salt = models.CharField(max_length=120, blank=False)
    register_date = models.DateField(auto_now_add=True, blank=False)

    #applications list
    favoriate_applications = models.ManyToManyField(Application, related_name='users_favorited')
    installed_applications = models.ManyToManyField(Application, related_name='users_installed')
    deleted_applications = models.ManyToManyField(Application, related_name='users_deleted')

    def __str__(self):
        return self.username

class Icon(models.Model):
    file_path = models.CharField(max_length=200, blank=False, unique=True)
    size = models.CharField(max_length=5, blank=False)
    application = models.ForeignKey(Application, on_delete=models.CASCADE)

    def clean(self):
        if not self.size in ('16', '24', '32', '48'):
            ValueError('Icon size must be 16, 24, 32, or 48 ')


class Media(models.Model):
    media_type = models.CharField(max_length=10, blank=False)
    file_path = models.CharField(max_length=200, blank=False)
    application = models.ForeignKey(Application, on_delete=models.CASCADE)

    def clean(self):
        if not self.media_type in ('picture', 'video'):
            raise ValueError('media type must be picture or video')


class Keyword(models.Model):
    name = models.CharField(max_length=80, blank=False, unique=True)
    application = models.ForeignKey(Application, on_delete=models.CASCADE)


class Review(models.Model):
    text = models.CharField(max_length=600, blank=False)
    rate = models.IntegerField(blank=False)
    # review_status = db.Column(db.String(30), blank=False, CheckConstraint(review_status in ('confirmed', 'removed', 'waiting')))
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    application = models.ForeignKey(Application, on_delete=models.CASCADE)

    def clean(self):
        if not self.rate in (1, 2, 3, 4, 5):
            raise ValueError('rate must be 1, 2, 3, 4 or 5')


    def __str__(self):
        return self.text[:20]

# payments
class UserPayment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    Application = models.ForeignKey(Application, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=10, decimal_places=2, blank=False)
    payment_type = models.CharField(max_length=30, blank=False)
    date = models.DateField(auto_now_add=True, blank=False)

    def clean(self):
        if not payment_type in ('inapp', 'buy'):
            raise ValidationError('payment_type must be inapp or buy')


class DeveloperInPayment(models.Model):
    developer = models.ForeignKey(Developer, on_delete=models.CASCADE)
    Application = models.ForeignKey(Application, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=10, decimal_places=2, blank=False)
    payment_type = models.CharField(max_length=30, blank=False)
    date = models.DateField(auto_now_add=True, blank=False)

    def clean(self):
        if not payment_type in ('inapp', 'buy', 'market', 'advertise', 'gift'):
            raise ValidationError('payment_type must be inapp, buy, market, advertise or gift')


class DeveloperOutPayment(models.Model):
    developer = models.ForeignKey(Developer, on_delete=models.CASCADE)
    Application = models.ForeignKey(Application, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=10, decimal_places=2, blank=False)
    payment_type = models.CharField(max_length=30, blank=False)
    date = models.DateField(auto_now_add=True, blank=False)

    def clean(self):
        if not payment_type in ('register', 'translation', 'advertise', 'cross-promotion'):
            raise ValidationError('payment_type must be register, translation, advertise, cross-promotion')