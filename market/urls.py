from django.urls import path

from . import views

app_name = 'market'
urlpatterns = [
    path('', views.index, name='index'),
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('lio/', views.lio, name='lio'),
    # market
    path('home/', views.market_home, name='market_home'),

    # developer
    path('developer_panel/', views.developer_panel_home),
    path('developer_panel/home', views.developer_panel_home, name='developer_panel_home'),
    path('developer_panel/applications', views.developer_panel_applications, name='developer_panel_applications'),
    path('developer_panel/applications/add', views.developer_panel_add_application, name='developer_panel_add_application'),
    path('developer_panel/device_catalog', views.developer_panel_device_catalog, name='developer_panel_device_catalog'),
    path('developer_panel/statistics', views.developer_panel_statistics, name='developer_panel_statistics'),
    path('developer_panel/financial', views.developer_panel_financial, name='developer_panel_financial'),
    path('developer_panel/support', views.developer_panel_support, name='developer_panel_support'),
    path('developer_panel/setting', views.developer_panel_setting, name='developer_panel_setting'),
]